# Array Backend

## Name
Array Backend Test



## Installation
For the first run: docker-compose up --build
for all after docker-compuse up is fine.

## Dependencies Installed
Bcrypt
Express
Sequelize
Pm2
Winston

## Design Decisions

## Scalability Choices
I went with PM2 to allow the server to run on different nodes withen the computer and also restart the server on exception crashes
In a larger company with the ability to scale quickly, i’d prefer to use multiple machines and make use of a web balancer like nginx or AWS ELB

## Why i’m using Promises in the services over async/await
I think it provides easier error handling 
Since it’s only going one level deep, the readability does not suffer like if it was multiple callbacks

## Seperating Logic between Controllers and Services
This was for readability and separating logic concerns
Controllers may need to draw data from multiple different sources and calling/modify that data inside of a controller can create a mess of code.

This can be taken one step forward as well. If not using Sequelize and having a self created ORM. The actual logic of getting the data from the DB can be abstracted to the model level, then the service level would then be used too modify the data if needed.

## Usage of a User Map
This was for authentication purposes.
After logging in, the server holds in memory, a hash table that keeps track of all users that are logged in. The format is {id => date()}
This can be further implemented by developing a cleanup function that would run periodically so that it doesn’t take up too much space if a user logs in and never logs out. 

## Postman Collection
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/55e488c9bea58ec034fc)

